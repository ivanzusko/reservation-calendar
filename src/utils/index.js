function groupByDay (data) {
  return data.map(company => {
    const groupedByDay = getGroupedByDay(company.time_slots)

    return {
      ...company,
      time_slots_by_day: groupedByDay
    }
  })

  function getGroupedByDay (data) {
    const week = getInitialWeek()

    return data.reduce((acc, item) => {
      const dayIdx = getDayIndex(item.start_time)
      acc[dayIdx].push(item)

      return acc
    }, week)

    function getInitialWeek () {
      return Array.from({ length: 7 }, () => [])
    }
  }
}

function getDayIndex (date) {
  return new Date(date).getDay()
}

function getDate (dateStr) {
  const d = new Date(dateStr)
  const year = d.getFullYear()
  const date = d.getDate()
  const month = d.getMonth() + 1

  return `${year}/${month}/${date}`
}

function getTime (data) {
  const d = new Date(data)
  const hours = prependZero(d.getHours())
  const minutes = prependZero(d.getMinutes())
  const time = `${hours}:${minutes}`

  return time

  function prependZero (number) {
    const num = String(number)
    return num.length === 1 ? `0${num}` : num
  }
}

function getTimeSlotRange (slot) {
  const start = slot.start_time
  const end = slot.end_time
  const time = `${getTime(start)} - ${getTime(end)}`

  return time
}

function getDayName (date) {
  const d = new Date(date)
  const result = new Intl.DateTimeFormat('en-US', { weekday: 'long' }).format(d)

  return result
}

export { groupByDay, getDate, getTime, getTimeSlotRange, getDayIndex, getDayName }
