import React from 'react'
import { useReservation } from '../../context/reservation-context'
import { getTimeSlotRange } from '../../utils'
import Name from './Name'
import SelectedTimeSlot from './SelectedTimeSlot'
import Week from './Week'
import './styles.css'

function Company ({ company }) {
  const [reservation, setReservation] = React.useState('')
  const { reservations, reservationsByCompany } = useReservation()

  React.useEffect(() => {
    const companyReservation = reservationsByCompany(company.id)

    if (companyReservation.length) {
      setReservation(getTimeSlotRange(companyReservation[0].timeSlot))
    } else {
      setReservation('')
    }
  }, [reservations, company, reservationsByCompany])

  return (
    <div className='Company'>
      <Name name={company.name} />
      <SelectedTimeSlot time={reservation} />
      <Week
        timeslots={company.time_slots_by_day}
        companyId={company.id}
      />
    </div>
  )
}

export default Company
