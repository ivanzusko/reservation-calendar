import { getTimeSlotRange } from '../../../utils'
import { useReservation } from '../../../context/reservation-context'
import TimeSlot from '../TimeSlot'

import './styles.css'

function Day ({ companyId, slots, name, onClick }) {
  const { handleReservation } = useReservation()

  const handleClick = (slot) => {
    handleReservation(companyId, slot)
  }

  return (
    <div className='Day'>
      <div className='Day-title'>{name}</div>
      {
        slots.map((slot, i) => {
          const blocked = slot.blocked
          const reserved = slot.reserved
          const timeSlot = getTimeSlotRange(slot)

          return (
            <TimeSlot
              key={`${name}_${timeSlot}`}
              time={timeSlot}
              reserve={() => handleClick(slot)}
              disabled={blocked}
              reserved={reserved}
            />
          )
        })
      }
    </div>
  )
}

export default Day
