import './styles.css'

function SelectedTimeSlot ({ time }) {
  return (
    <div className='Selected-time-slot'>{time}</div>
  )
}

export default SelectedTimeSlot
