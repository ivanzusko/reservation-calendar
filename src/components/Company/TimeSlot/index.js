import './styles.css'

function TimeSlot ({ time, reserve, disabled, reserved }) {
  return (
    <button
      className={`Time-slot${reserved ? ' Time-slot--reserved' : ''}`}
      onClick={reserve}
      disabled={disabled}
    >
      {time}
    </button>
  )
}

export default TimeSlot
