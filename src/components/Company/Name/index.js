import './styles.css'

function Name ({ name }) {
  return (
    <div className='Company-name'>{name}</div>
  )
}

export default Name
