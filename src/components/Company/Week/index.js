import Day from '../Day'
import { getDayName } from '../../../utils'
import './styles.css'

function Week ({ companyId, timeslots, reserve }) {
  return (
    <div className='Week'>
      {
        timeslots.map((slots) => {
          if (!slots.length) return null

          const dayName = getDayName(slots[0].start_time)

          return (
            <Day
              slots={slots}
              name={dayName}
              key={dayName}
              onClick={reserve}
              companyId={companyId}
            />
          )
        })
      }

    </div>
  )
}

export default Week
