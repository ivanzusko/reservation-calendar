import { useTimeSlots } from '../../context/time-slots-context'
import Company from '../Company'
import './App.css'

function App () {
  const { companies } = useTimeSlots()

  if (!companies) return null

  return (
    <div className='App'>
      {
        companies.map(company => (
          <Company key={company.id} company={company} />
        ))
      }
    </div>
  )
}

export default App
