import React, { createContext, useState, useContext } from 'react'

const ReservationContext = createContext()

function ReservationProvider (props) {
  const [reservations, setReservations] = useState({})
  const [reservation, setReservation] = useState(null)

  const handleReservation = (companyId, timeSlot) => {
    if (Object.prototype.hasOwnProperty.call(reservations, timeSlot.start_time)) {
      delete reservations[timeSlot.start_time]

      setReservation({
        companyId,
        timeSlot,
        toDelete: true
      })
    } else {
      setReservations({
        ...reservations,
        [timeSlot.start_time]: {
          companyId,
          timeSlot
        }
      })

      setReservation({
        companyId,
        timeSlot
      })
    }
  }

  const reservationsByCompany = id => {
    const result = []

    for (const res in reservations) {
      if (reservations[res].companyId === id) result.push(reservations[res])
    }

    return result
  }

  return (
    <ReservationContext.Provider
      value={{
        reservation,
        reservations,
        handleReservation,
        reservationsByCompany
      }}
    >
      {props.children}
    </ReservationContext.Provider>
  )
}

const useReservation = () => useContext(ReservationContext)

export { ReservationProvider, useReservation }
