import React, { createContext, useEffect, useState, useContext } from 'react'
import { useReservation } from './reservation-context'
import { groupByDay, getDayIndex } from '../utils'

const TimeSlotsContext = createContext()

function TimeSlotsProvider (props) {
  const [slots, setSlots] = useState(null)
  const [shouldFetchSlots, setShouldFetchSlots] = useState(true)
  const { reservation, reservations } = useReservation()

  useEffect(() => {
    if (shouldFetchSlots) {
      getTimeSlots()
        .then(data => {
          setSlots(groupByDay(data))
          setShouldFetchSlots(false)
        })
    }
  }, [shouldFetchSlots])

  useEffect(() => {
    if (slots && reservation) {
      let updatedSlots

      if (reservation.toDelete) {
        const dayIdx = getDayIndex(reservation.timeSlot.start_time)

        // clean all
        updatedSlots = slots.map(company => {
          const upd = company.time_slots_by_day[dayIdx].map(slot => {
            const { blocked, reserved, ...updatedSlot } = slot
            return updatedSlot
          })

          const timeSlotsByDay = [...company.time_slots_by_day]

          timeSlotsByDay[dayIdx] = upd

          return {
            ...company,
            time_slots_by_day: timeSlotsByDay
          }
        })

        // repaint reservations
        for (const item in reservations) {
          updatedSlots = getUpdatedTimeSlots(updatedSlots, reservations[item])
        }
      } else {
        updatedSlots = getUpdatedTimeSlots(slots, reservation)
      }

      setSlots(updatedSlots)
    }
  }, [reservation])

  return (
    <TimeSlotsContext.Provider
      value={{
        companies: slots
      }}
    >
      {props.children}
    </TimeSlotsContext.Provider>
  )
}

async function getTimeSlots () {
  try {
    // @TODO: implement mock server
    // const result = await window.fetch("/timeslots")
    // const responseData = result.json()
    const responseData = mockData

    return responseData
  } catch (error) {
    return error
  }
}

const useTimeSlots = () => useContext(TimeSlotsContext)

export { TimeSlotsProvider, useTimeSlots }

function getUpdatedTimeSlots (slots, reservation) {
  return slots.map(slot => {
    const dayIdx = getDayIndex(reservation.timeSlot.start_time)
    const timeSlots = slot.time_slots_by_day[dayIdx]

    if (slot.id !== reservation.companyId) {
      slot.time_slots_by_day[dayIdx] = addBlocked(timeSlots, reservation)
    } else {
      slot.time_slots_by_day[dayIdx] = addReserved(timeSlots, reservation)
    }

    return slot
  })
}

function addBlocked (timeSlots, reservation) {
  return timeSlots.map(timeSlot => {
    if (
      (
        timeSlot.start_time < reservation.timeSlot.end_time &&
        timeSlot.start_time >= reservation.timeSlot.start_time
      ) ||
      (
        timeSlot.end_time > reservation.timeSlot.start_time &&
        timeSlot.start_time < reservation.timeSlot.start_time
      )
    ) {
      return {
        ...timeSlot,
        blocked: true
      }
    }

    return timeSlot
  })
}

function addReserved (slots, reservation) {
  return slots.map(slot => {
    if (slot.start_time === reservation.timeSlot.start_time) {
      slot.reserved = true
    }

    return slot
  })
}

const mockData = [
  {
    id: 1,
    name: 'Company 1',
    type: 'company',
    time_slots: [
      {
        start_time: '2018-07-09T08:00:00.000+02:00',
        end_time: '2018-07-09T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T08:30:00.000+02:00',
        end_time: '2018-07-09T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T09:00:00.000+02:00',
        end_time: '2018-07-09T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T09:30:00.000+02:00',
        end_time: '2018-07-09T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T10:00:00.000+02:00',
        end_time: '2018-07-09T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T10:30:00.000+02:00',
        end_time: '2018-07-09T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T11:00:00.000+02:00',
        end_time: '2018-07-09T12:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T11:30:00.000+02:00',
        end_time: '2018-07-09T13:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T12:00:00.000+02:00',
        end_time: '2018-07-09T13:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T12:30:00.000+02:00',
        end_time: '2018-07-09T14:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T08:00:00.000+02:00',
        end_time: '2018-07-10T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T08:30:00.000+02:00',
        end_time: '2018-07-10T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T09:00:00.000+02:00',
        end_time: '2018-07-10T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T09:30:00.000+02:00',
        end_time: '2018-07-10T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T10:00:00.000+02:00',
        end_time: '2018-07-10T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T10:30:00.000+02:00',
        end_time: '2018-07-10T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T11:00:00.000+02:00',
        end_time: '2018-07-10T12:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T11:30:00.000+02:00',
        end_time: '2018-07-10T13:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T12:00:00.000+02:00',
        end_time: '2018-07-10T13:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T12:30:00.000+02:00',
        end_time: '2018-07-10T14:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T13:00:00.000+02:00',
        end_time: '2018-07-10T14:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T13:30:00.000+02:00',
        end_time: '2018-07-10T15:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T14:00:00.000+02:00',
        end_time: '2018-07-10T15:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T14:30:00.000+02:00',
        end_time: '2018-07-10T16:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T08:00:00.000+02:00',
        end_time: '2018-07-12T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T08:30:00.000+02:00',
        end_time: '2018-07-12T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T09:00:00.000+02:00',
        end_time: '2018-07-12T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T09:30:00.000+02:00',
        end_time: '2018-07-12T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T10:00:00.000+02:00',
        end_time: '2018-07-12T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T10:30:00.000+02:00',
        end_time: '2018-07-12T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T17:00:00.000+02:00',
        end_time: '2018-07-12T18:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T17:30:00.000+02:00',
        end_time: '2018-07-12T19:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T08:00:00.000+02:00',
        end_time: '2018-07-13T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T08:30:00.000+02:00',
        end_time: '2018-07-13T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T09:00:00.000+02:00',
        end_time: '2018-07-13T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T09:30:00.000+02:00',
        end_time: '2018-07-13T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T10:00:00.000+02:00',
        end_time: '2018-07-13T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T10:30:00.000+02:00',
        end_time: '2018-07-13T12:00:00.000+02:00'
      }
    ]
  },
  {
    id: 2,
    name: 'Company 2',
    type: 'company',
    time_slots: [
      {
        start_time: '2018-07-09T08:00:00.000+02:00',
        end_time: '2018-07-09T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T08:30:00.000+02:00',
        end_time: '2018-07-09T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T09:00:00.000+02:00',
        end_time: '2018-07-09T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T09:30:00.000+02:00',
        end_time: '2018-07-09T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T10:00:00.000+02:00',
        end_time: '2018-07-09T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T10:30:00.000+02:00',
        end_time: '2018-07-09T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T11:00:00.000+02:00',
        end_time: '2018-07-09T12:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T11:30:00.000+02:00',
        end_time: '2018-07-09T13:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T12:00:00.000+02:00',
        end_time: '2018-07-09T13:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T12:30:00.000+02:00',
        end_time: '2018-07-09T14:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T08:00:00.000+02:00',
        end_time: '2018-07-10T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T08:30:00.000+02:00',
        end_time: '2018-07-10T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T09:00:00.000+02:00',
        end_time: '2018-07-10T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T09:30:00.000+02:00',
        end_time: '2018-07-10T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T10:00:00.000+02:00',
        end_time: '2018-07-10T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T10:30:00.000+02:00',
        end_time: '2018-07-10T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T11:00:00.000+02:00',
        end_time: '2018-07-10T12:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T11:30:00.000+02:00',
        end_time: '2018-07-10T13:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T12:00:00.000+02:00',
        end_time: '2018-07-10T13:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T12:30:00.000+02:00',
        end_time: '2018-07-10T14:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T13:00:00.000+02:00',
        end_time: '2018-07-10T14:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T13:30:00.000+02:00',
        end_time: '2018-07-10T15:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T14:00:00.000+02:00',
        end_time: '2018-07-10T15:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T14:30:00.000+02:00',
        end_time: '2018-07-10T16:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T08:00:00.000+02:00',
        end_time: '2018-07-12T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T08:30:00.000+02:00',
        end_time: '2018-07-12T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T09:00:00.000+02:00',
        end_time: '2018-07-12T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T09:30:00.000+02:00',
        end_time: '2018-07-12T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T10:00:00.000+02:00',
        end_time: '2018-07-12T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T10:30:00.000+02:00',
        end_time: '2018-07-12T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T17:00:00.000+02:00',
        end_time: '2018-07-12T18:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T17:30:00.000+02:00',
        end_time: '2018-07-12T19:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T08:00:00.000+02:00',
        end_time: '2018-07-13T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T08:30:00.000+02:00',
        end_time: '2018-07-13T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T09:00:00.000+02:00',
        end_time: '2018-07-13T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T09:30:00.000+02:00',
        end_time: '2018-07-13T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T10:00:00.000+02:00',
        end_time: '2018-07-13T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T10:30:00.000+02:00',
        end_time: '2018-07-13T12:00:00.000+02:00'
      }
    ]
  },
  {
    id: 3,
    name: 'Company 3',
    type: 'company',
    time_slots: [
      {
        start_time: '2018-07-09T08:00:00.000+02:00',
        end_time: '2018-07-09T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T08:30:00.000+02:00',
        end_time: '2018-07-09T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T09:00:00.000+02:00',
        end_time: '2018-07-09T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T09:30:00.000+02:00',
        end_time: '2018-07-09T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T10:00:00.000+02:00',
        end_time: '2018-07-09T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T10:30:00.000+02:00',
        end_time: '2018-07-09T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T11:00:00.000+02:00',
        end_time: '2018-07-09T12:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T11:30:00.000+02:00',
        end_time: '2018-07-09T13:00:00.000+02:00'
      },
      {
        start_time: '2018-07-09T12:00:00.000+02:00',
        end_time: '2018-07-09T13:30:00.000+02:00'
      },
      {
        start_time: '2018-07-09T12:30:00.000+02:00',
        end_time: '2018-07-09T14:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T08:00:00.000+02:00',
        end_time: '2018-07-10T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T08:30:00.000+02:00',
        end_time: '2018-07-10T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T09:00:00.000+02:00',
        end_time: '2018-07-10T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T09:30:00.000+02:00',
        end_time: '2018-07-10T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T10:00:00.000+02:00',
        end_time: '2018-07-10T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T10:30:00.000+02:00',
        end_time: '2018-07-10T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T11:00:00.000+02:00',
        end_time: '2018-07-10T12:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T11:30:00.000+02:00',
        end_time: '2018-07-10T13:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T12:00:00.000+02:00',
        end_time: '2018-07-10T13:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T12:30:00.000+02:00',
        end_time: '2018-07-10T14:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T13:00:00.000+02:00',
        end_time: '2018-07-10T14:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T13:30:00.000+02:00',
        end_time: '2018-07-10T15:00:00.000+02:00'
      },
      {
        start_time: '2018-07-10T14:00:00.000+02:00',
        end_time: '2018-07-10T15:30:00.000+02:00'
      },
      {
        start_time: '2018-07-10T14:30:00.000+02:00',
        end_time: '2018-07-10T16:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T08:00:00.000+02:00',
        end_time: '2018-07-12T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T08:30:00.000+02:00',
        end_time: '2018-07-12T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T09:00:00.000+02:00',
        end_time: '2018-07-12T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T09:30:00.000+02:00',
        end_time: '2018-07-12T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T10:00:00.000+02:00',
        end_time: '2018-07-12T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T10:30:00.000+02:00',
        end_time: '2018-07-12T12:00:00.000+02:00'
      },
      {
        start_time: '2018-07-12T17:00:00.000+02:00',
        end_time: '2018-07-12T18:30:00.000+02:00'
      },
      {
        start_time: '2018-07-12T17:30:00.000+02:00',
        end_time: '2018-07-12T19:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T08:00:00.000+02:00',
        end_time: '2018-07-13T09:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T08:30:00.000+02:00',
        end_time: '2018-07-13T10:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T09:00:00.000+02:00',
        end_time: '2018-07-13T10:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T09:30:00.000+02:00',
        end_time: '2018-07-13T11:00:00.000+02:00'
      },
      {
        start_time: '2018-07-13T10:00:00.000+02:00',
        end_time: '2018-07-13T11:30:00.000+02:00'
      },
      {
        start_time: '2018-07-13T10:30:00.000+02:00',
        end_time: '2018-07-13T12:00:00.000+02:00'
      }
    ]
  }
]
